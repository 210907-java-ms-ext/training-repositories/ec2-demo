# ec2-demo

### Set up your own environment on an EC2:


1. Create an instance of an EC2 (Amazon Linux 2 AMI)

2. Connect to you EC2 using ssh.

   > ssh -i [pem file] ec2-user@[public ip]

3. Install git

   > sudo yum install git -y

4. Clone this repository onto your EC2

   > git clone https://gitlab.com/210907-java-ms-ext/training-repositories/ec2-demo

5. Navigate into the project folder and execute install script. Select Java 8 when prompted.

   > cd devops-script
   > sh install.sh

You should now have git, tomcat, java and maven installed.

### Building Your App

### Running Your App On Tomcat 

### Setting Environment Variables for Tomcat
